package org.usfirst.frc3610.islanders2019;


/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {

  // Drivetrain Subsystem
public static final int DRIVETRAIN_RIGHT_F = 0;
public static final int DRIVETRAIN_RIGHT_R = 1;
public static final int DRIVETRAIN_LEFT_F = 2;
public static final int DRIVETRAIN_LEFT_R = 3;
public static final int DRIVESTICK = 0;
public static final int XDRIVEAXIS = 5;
public static final int YDRIVEAXIS = 1;
public static final int ZROTATION = 1;

public static final int AUTOBACKBTN = 1;
public static final int AUTOFORWARDBTN = 2;
public static final int STRAIGHTFORWARDBTN = 3;
public static final double AUTOMOVEDURATION = 0.25; //in seconds
public static final double AUTOSPEED = 100;


  // Winch
public static final int WINCH_RIGHT = 4;
public static final int WINCH_LEFT = 9;
public static final int WINCHSTICK = 1;
public static final int WINCHSTICKLOC = 5;
public static final int POS1BTNPIN = 6;
public static final int POS2BTNPIN = 7;
public static final int POS3BTNPIN = 9;
public static final int POS1SWITCH = 0;
public static final int POS2SWITCH = 1;
public static final int POS3SWITCH = 2;
public static final int REDLED = 6;
public static final int BLUELED = 7;
public static final int GREENLED = 8;

  // Pneumatics
  //INCOMPLETE - ADD PINS WHEN WIRED!
public static final int PNEU_SOL_1_1 = 0;
public static final int PNEU_SOL_1_2 = 1;
public static final int PNEU_SOL_2_1 = 2;
public static final int PNEU_SOL_2_2 = 3;
public static final int PNEU_SOL_3_1 = 4;
public static final int PNEU_SOL_3_2 = 5;
public static final int FRONTUPBTN = 4;  //Y
public static final int FRONTDOWNBTN = 3; //X
public static final int BACKUPBTN = 2; //B
public static final int BACKDOWNBTN = 1; //A
public static final int FRONTDOWNSLOWBTN = 5;  //Left Bumper - using for BothUp (prototype)
public static final int FRONTSTOPBTN = 6;  //Right Bumper - using for BothDown (prototype)

public static final double TILTTIME = 0.2; //Time to allow front piston to drop
//public static final int SOLENOIDSTATBTN = 7;

//Foot Control Variables
public static final int FOOTCONTROL1 = 8;
public static final int FOOTCONTROL2 = 7;
public static final int FOOTSTICK_R = 2;
public static final int FOOTSTICK_L = 3;

//Troublshooting Buttons
public static final int SOLENOIDSTATUS = 1; //A on the DriveStick



}