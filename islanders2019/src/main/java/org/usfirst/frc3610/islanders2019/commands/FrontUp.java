
package org.usfirst.frc3610.islanders2019.commands;
import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc3610.islanders2019.Robot;
/**
 *
 */
public class FrontUp extends Command {

    public FrontUp() {
       requires(Robot.pneumatics);
       
    }

    // Called just before this Command runs the first time
    @Override
    protected void initialize() {
        System.out.println("Front Up Called");
        Robot.pneumatics.frontPistonExtend();
         
    }

    // Called repeatedly when this Command is scheduled to run
    @Override
    protected void execute() {
   }

    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    @Override
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted() {
    }
}
