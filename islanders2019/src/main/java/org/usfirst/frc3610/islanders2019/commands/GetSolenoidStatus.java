
package org.usfirst.frc3610.islanders2019.commands;
import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc3610.islanders2019.Robot;
/**
 *
 */
public class GetSolenoidStatus extends Command {

    public GetSolenoidStatus() {
       requires(Robot.pneumatics);
       
    }

    // Called just before this Command runs the first time
    @Override
    protected void initialize() {
        System.out.println("Solenoid Status:");
        System.out.print("Front Solenoid: ");
        System.out.println(Robot.pneumatics.getFrontSolenoidStatus());
        System.out.print("Back Solenoid: ");
        System.out.println(Robot.pneumatics.getBackSolenoidStatus());
        System.out.print("Control Solenoid: ");
        System.out.println(Robot.pneumatics.getControlSolenoidStatus());
    }

    // Called repeatedly when this Command is scheduled to run
    @Override
    protected void execute() {
        
    }

    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    @Override
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted() {
    }
}
